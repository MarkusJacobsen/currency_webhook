package main

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
)

//DB -- Struct for representing a DB with a collection
type DB struct {
	Host               string
	DataBaseName       string
	DataBaseCollection string
}

//TestConnection -- Creates a connection with the database
func (db *DB) TestConnection() {
	conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	//NOP
}

//SaveDBPost -- Adds a webhook into the database
func (db *DB) SaveDBPost(p interface{}) {
	conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	err = conn.DB(db.DataBaseName).C(db.DataBaseCollection).Insert(p)

	if err != nil {
		fmt.Printf("Could not register the post: %s", err.Error())
	}
}

//Count -- Return the count in the DB collection
func (db *DB) Count() int {
	conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	count, err := conn.DB(db.DataBaseName).C(db.DataBaseCollection).Count()
	if err != nil {
		fmt.Printf("Error getting the count: %s", err.Error())
		return -1
	}

	return count

}

/**
* Finds a post based in a ObjectId
 */
func (db *DB) findPost(field string, query string, p *payload) error {
	conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	if bson.IsObjectIdHex(query) {
		return conn.DB(db.DataBaseName).C(db.DataBaseCollection).Find(bson.M{field: bson.ObjectIdHex(query)}).One(&p)
	}
	return conn.DB(db.DataBaseName).C(db.DataBaseCollection).Find(bson.M{field: query}).One(&p)
}

/**
Deletes a post from the DB based on some ObjectId
*/
func (db *DB) deletePost(id string) error {
	conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	err = conn.DB(db.DataBaseName).C(db.DataBaseCollection).RemoveId(bson.ObjectIdHex(id))
	if err != nil {
		return err
	}
	return nil
}

func (db *DB) getAverage(base string, target string) float64 {
	conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	var result []float64

	pastTime := time.Now().AddDate(0, 0, -7).Format("2006-01-02") // Get date seven days ago
	conn.DB(db.DataBaseName).C(db.DataBaseCollection).Find(bson.M{"rates": target, "time": bson.M{"$gt": pastTime}}).All(&result)

	var sum float64

	for _, tall := range result {
		sum += tall
	}
	return sum / (float64(len(result)))

	//NOT WORKING
	/*conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()


	var result [] Currency

	pastTime := time.Now().AddDate(0, 0, -7).Format("2006-01-02")  // Get date seven days ago
	conn.DB(db.DataBaseName).C(db.DataBaseCollection).Find(bson.M{"time" : bson.M{"$gt" : pastTime}}).All(&result)

	var sum float64 = 0.0
	var targets [] float64

	for c := range result {
		for k, v := range result[c].Rates {
			if k == target {
				targets[c] = v
			}
		}
	}

	if base != "EUR" {
		var bases [] float64
		var conversion [] float64

		for c := range result {
			for k, v := range result[c].Rates {
				if k == base {
					bases[c] = v
				}
			}
		}

		for v := range bases {
			conversion[v] = targets[v] / bases[v]
		}
		for _, tall := range conversion {
			sum += tall
		}
		return sum/(float64(len(result)))
	} else {
		for _, tall := range targets {
			sum += tall

		}
		return sum/(float64(len(result)))
	}*/

}

func (db *DB) getLast(p *Currency) {
	conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	conn.DB(db.DataBaseName).C(db.DataBaseCollection).Find(nil).Sort("$natural").One(&p)
}

func (db *DB) calculateLatest(base string, target string, p Currency) float64 {
	if base == "EUR" {
		return retrieveExchangeRate(target, p)
	}

	var baseValue float64
	var targetValue float64
	for k, v := range p.Rates {
		if k == base {
			baseValue = v
		}
	}
	for k, v := range p.Rates {
		if k == target {
			targetValue = v
		}
	}

	return targetValue / baseValue
}

func (db *DB) findAndInvoke(query *InvokeJSON) []payload {
	conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	var results []payload
	conn.DB(db.DataBaseName).C(db.DataBaseCollection).Find(bson.M{"basecurrency": query.BaseCurrency, "targetcurrency": query.TargetCurrency, "mintriggervalue": query.MinTriggerValue, "maxtriggervalue": query.MaxTriggerValue}).All(&results)
	return results
}

func (db *DB) invokeAll() []payload {
	conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	var results []payload
	conn.DB(db.DataBaseName).C(db.DataBaseCollection).Find(nil).All(&results)
	return results
}
