package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"sync"
	"time"
)

/**
Struct to register webhooks
*/
type payload struct {
	ID              bson.ObjectId `bson:"_id,omitempty" json:"id"`
	WebhookURL      string        `json:"webhookURL"`
	BaseCurrency    string        `json:"baseCurrency"`
	TargetCurrency  string        `json:"targetCurrency"`
	MinTriggerValue float64       `json:"minTriggerValue"`
	MaxTriggerValue float64       `json:"maxTriggerValue"`
}

//InvokeJSON -- used to invoke webhooks, JSON request
type InvokeJSON struct {
	BaseCurrency    string  `json:"baseCurrency"`
	TargetCurrency  string  `json:"targetCurrency"`
	CurrentRate     float64 `json:"currentRate"`
	MinTriggerValue float64 `json:"minTriggerValue"`
	MaxTriggerValue float64 `json:"maxTriggerValue"`
}

/**
Used in getting the id from a URL
*/
type id struct {
	ID string `json:"id"`
}

/**
Sets up and handles all routes in the site
*/
func routing(w http.ResponseWriter, r *http.Request) {
	db := setUpDB("mongodb://Users:12345@ds243805.mlab.com:43805/markusja-currency_db", "markusja-currency_db", "webhooks")
	//getDataFromFixer()

	switch r.Method {
	case "POST":
		handlePOST(w, r, db)
		break
	case "GET":
		handleGET(w, r, db)
		break
	case "DELETE":
		handleDELETE(w, r, db)
		break
	default:
		fmt.Fprint(w, "Hello from go\n")
		break
	}

	//Try to listen to traffic
}

/**
Handles the POST routing
*/
func handlePOST(w http.ResponseWriter, r *http.Request, db *DB) {
	switch r.URL.Path {
	case "/latest":
		handlePOSTLatest(w, r, db)
		break
	case "/average":
		handlePOSTAverage(w, r, db)
		break
	case "/":
		handlePOSTRoot(w, r, db)
		break
	case "/webhookUrl":
		handlePOSTWebhook(w, r, db)
		break
	}
}

/**
Handle GET request
*/
func handleGET(w http.ResponseWriter, r *http.Request, db *DB) {
	switch r.URL.Path {
	case "/evaluationTrigger":
		handleGETEvaluationTrigger(w, r, db)
		break
	default:
		id := getIDFromURL(r)
		p := payload{}
		err := db.findPost("_id", id.ID, &p)
		if err != nil {
			fmt.Fprintf(w, "Could not find webhook: %s", err.Error())
		} else {
			prettyPrintPayload(&p, w)
		}
	}
}

func handleGETEvaluationTrigger(w http.ResponseWriter, r *http.Request, db *DB) {
	webhooks := db.invokeAll()
	if webhooks != nil {
		var wg sync.WaitGroup
		for hook := range webhooks {
			wg.Add(1)
			go func(hook int) {
				defer wg.Done()
				res := webhooks[hook].Invoke(w)
				fmt.Fprintln(w, res)
			}(hook)
		}
		wg.Wait()
	} else {
		fmt.Fprint(w, "No matching webhooks found")
	}
}

/**
Handle DELETE request
*/
func handleDELETE(w http.ResponseWriter, r *http.Request, db *DB) {
	id := getIDFromURL(r)
	err := db.deletePost(id.ID)
	if err != nil {
		fmt.Fprintf(w, "Could not delete webhook with this id: %s", err.Error())
	} else {
		fmt.Fprint(w, "true")
	}
}

/**
Handle /latest request
*/
func handlePOSTLatest(w http.ResponseWriter, r *http.Request, db *DB) {
	currency := BaseTargetInvoke{}
	err := decodeJSON(r, &currency)

	if err != nil {
		fmt.Fprintf(w, "Could not decode json: %s", err.Error())
	} else {

		db.DataBaseCollection = "currency_latest"
		p := Currency{}
		db.getLast(&p)
		latest := db.calculateLatest(currency.BaseCurrency, currency.TargetCurrency, p)
		fmt.Fprint(w, latest)
	}
}

/**
Handle /average request
*/
func handlePOSTAverage(w http.ResponseWriter, r *http.Request, db *DB) {
	currency := BaseTargetInvoke{}
	err := decodeJSON(r, &currency)

	if err != nil {
		fmt.Fprintf(w, "Could not decode json: %s", err.Error())
	} else {
		db.DataBaseCollection = "currency_latest"
		fmt.Fprint(w, db.getAverage(currency.BaseCurrency, currency.TargetCurrency))
	}
}

/**
Handle / POST request - save webhook case
*/
func handlePOSTRoot(w http.ResponseWriter, r *http.Request, db *DB) {
	p := payload{}
	err := decodeJSON(r, &p)
	p.ID = bson.NewObjectIdWithTime(time.Now())

	if err != nil {
		fmt.Fprintf(w, "Error while decoding JSON: %s", err.Error())
	} else {
		db.SaveDBPost(&p)
		fmt.Fprint(w, p.ID.Hex())
	}
}

/**
Handle /webhookURL request
*/
func handlePOSTWebhook(w http.ResponseWriter, r *http.Request, db *DB) {
	p := InvokeJSON{}
	err := decodeJSON(r, &p)

	if err != nil {
		fmt.Fprintf(w, "Error while decoding JSON: %s", err.Error())
	} else {
		webhooks := db.findAndInvoke(&p)
		if webhooks != nil {
			var wg sync.WaitGroup
			for hook := range webhooks {
				wg.Add(1)
				go func(hook int) {
					defer wg.Done()
					res := webhooks[hook].Invoke(w)
					fmt.Fprintln(w, res)
				}(hook)
			}
			wg.Wait()
		} else {
			fmt.Fprint(w, "No matching webhooks found")
		}
	}
}

/**
create a post request to the webhookURL
*/
func (hook *payload) Invoke(w http.ResponseWriter) string {
	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(hook)
	if !isValidURL(hook.WebhookURL) {
		hook.WebhookURL = "http://www." + hook.WebhookURL
	}
	res, err := http.Post(hook.WebhookURL, "application/json", b)
	if err != nil {
		return hook.WebhookURL + "returned: no content"
	}
	return hook.WebhookURL + " returned: " + res.Status
}

/**
Prints JSON with indents and newlines
*/
func prettyPrintPayload(p interface{}, w http.ResponseWriter) {
	b, err := json.MarshalIndent(p, "", "  ")
	if err != nil {
		fmt.Fprintf(w, "Could not print JSON: %s", err.Error())
	}
	w.Header().Set("Content-type", "application/json")
	fmt.Fprintf(w, "%s\n", b)
}

/**
Returns a instance of id struct with a numchar
*/
func getIDFromURL(r *http.Request) id {
	reg := regexp.MustCompile("[^a-zA-Z0-9]+")
	id := id{reg.ReplaceAllString(r.URL.Path, "")}
	return id
}

// https://golangcode.com/how-to-check-if-a-string-is-a-url/
func isValidURL(link string) bool {
	_, err := url.ParseRequestURI(link)
	if err != nil {
		return false
	}
	return true
}

/**
Decodes a JSON into a interface from a http request body
*/
func decodeJSON(r *http.Request, p interface{}) error {
	return json.NewDecoder(r.Body).Decode(&p)
}

/**
Returns a instance of DB struct
*/
func setUpDB(host string, dataBaseName string, databaseCollection string) *DB {
	db := DB{
		Host:               host,
		DataBaseName:       dataBaseName,
		DataBaseCollection: databaseCollection,
	}
	return &db
}

/**
Counts down X seconds and after the time is up, fetch latest results from fixer
*/
func countDown(countDownInSeconds int) {

	t := time.NewTicker(time.Duration(countDownInSeconds) * time.Second)
	// or just use the usual for { select {} } idiom of receiving from a channel
	for now := range t.C {
		fmt.Print(now)
		db := setUpDB("mongodb://Users:12345@ds243805.mlab.com:43805/markusja-currency_db", "markusja-currency_db", "currency_latest")
		p := Currency{}
		getData("http://api.fixer.io/latest?base=EUR", &p)
		db.SaveDBPost(&p)
	}
}

/**
Main Starts the counter and start to listen and serve the site
*/
func main() {
	go countDown(86400)

	online := true

	if online {
		port := os.Getenv("PORT")
		if port == "" {
			log.Fatal("$PORT must be set")
		}
		http.HandleFunc("/", routing)
		panic(http.ListenAndServe(":"+port, nil))
	} else {
		http.HandleFunc("/", routing)
		panic(http.ListenAndServe(":8080", nil))
	}

	//

}
