package main

import (
	"encoding/json"
	"net/http"
)

//Currency -- Struct for holding info from fixer.io
type Currency struct {
	Base  string             `json:"base"`
	Date  string             `json:"date"`
	Rates map[string]float64 `json:"rates"`
}

//BaseTargetInvoke -- Used in invoking "latest" and "average"
type BaseTargetInvoke struct {
	BaseCurrency   string `json:"baseCurrency"`
	TargetCurrency string `json:"targetCurrency"`
}

/**
Does a get request to some site and decodes the result into a interface
*/
func getData(URL string, currency interface{}) error {
	resp, err := http.Get(URL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return json.NewDecoder(resp.Body).Decode(&currency)
}

/**
Checks a map if a key is present, if return value
*/
func retrieveExchangeRate(target string, p Currency) float64 {
	for k, v := range p.Rates {
		if k == target {
			return v
		}
	}
	return 0
}
