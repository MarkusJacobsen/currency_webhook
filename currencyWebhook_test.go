package main

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
)

func setUpTestDB() *DB {
	db := DB{
		Host:               "mongodb://localhost",
		DataBaseName:       "markusja_testing",
		DataBaseCollection: "currency_db",
	}
	return &db
}

func (db *DB) tearDownCollection() {
	conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	err = conn.DB(db.DataBaseName).C(db.DataBaseCollection).DropCollection()
	if err != nil {
		fmt.Printf("Could not tear down collection: %s", err.Error())
	}
}

func (db *DB) populateCurrency() {
	conn, err := mgo.Dial(db.Host)
	if err != nil {
		panic(err)
	}

	defer conn.Close()

	p := Currency{}
	getData("http://www.api.fixer.io/latest?base=EUR", &p)
	db.SaveDBPost(p)

}

func TestDB_TestConnectionPanic(t *testing.T) {
	db := setUpTestDB()
	db.Host = "mongodb://local"

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	db.TestConnection()
}

func TestDB_CountPanic(t *testing.T) {
	db := setUpTestDB()
	db.Host = "mongodb://local"

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	db.Count()
}

func TestDB_SaveDBPostPanic(t *testing.T) {
	db := setUpTestDB()
	db.Host = "mongodb://local"

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	p := payload{}
	db.SaveDBPost(&p)
}

func TestDB_DeletePostPanic(t *testing.T) {
	db := setUpTestDB()
	db.Host = "mongodb://local"

	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	id := id{}
	db.deletePost(id.ID)
}

func TestSaveWebhook(t *testing.T) {
	db := setUpTestDB()

	p := payload{
		WebhookURL:      "someURL.com",
		BaseCurrency:    "EUR",
		TargetCurrency:  "NOK",
		MinTriggerValue: 2.3,
		MaxTriggerValue: 4.5,
	}

	db.TestConnection()
	db.SaveDBPost(p)

	if db.Count() != 1 {
		t.Fail()
	}
	db.tearDownCollection()
}

func TestDeleteWebhook(t *testing.T) {
	db := setUpTestDB()

	p := payload{
		ID:              bson.NewObjectId(),
		WebhookURL:      "someURL.com",
		BaseCurrency:    "EUR",
		TargetCurrency:  "NOK",
		MinTriggerValue: 2.3,
		MaxTriggerValue: 4.5,
	}

	db.TestConnection()
	db.SaveDBPost(p)
	db.deletePost(p.ID.Hex())
	if db.Count() != 0 {
		t.Fail()
	}
	db.tearDownCollection()
}

func TestFindPost(t *testing.T) {
	db := setUpTestDB()

	p := payload{
		ID:              bson.NewObjectId(),
		WebhookURL:      "someURL.com",
		BaseCurrency:    "EUR",
		TargetCurrency:  "NOK",
		MinTriggerValue: 2.3,
		MaxTriggerValue: 4.5,
	}

	db.TestConnection()
	db.SaveDBPost(p)

	r := payload{}
	err := db.findPost("_id", p.ID.Hex(), &r)

	if err != nil {
		t.Fail()
	}
	db.tearDownCollection()
}

func TestGetIdFromURL(t *testing.T) {
	req := http.Request{Method: "GET"}
	req.URL, _ = url.Parse("http://localhost/jieDJW321336de")
	expected := "jieDJW321336de"

	id := getIDFromURL(&req)
	if id.ID != expected {
		t.Failed()
	}
}

func TestSetUpDB(t *testing.T) {
	host := "localhost"
	dbName := "db"
	dbCollection := "db_test"

	db := setUpDB(host, dbName, dbCollection)

	if db.Host != host && db.DataBaseName != dbName && db.DataBaseCollection != dbCollection {
		t.Failed()
	}
}

func TestGET(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(routing))
	defer ts.Close()

	id := bson.NewObjectId()
	res := ts.URL + "/" + id.Hex()
	resp, err := http.Get(res)
	if err != nil {
		t.Errorf("Error making the GET request, %s", err)
	}
	if resp.StatusCode != http.StatusOK {
		t.Errorf("For route: %s, expected StatusCode %d, received %d", res,
			http.StatusOK, resp.StatusCode)
		return
	}
}

func TestPOST(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(routing))
	defer ts.Close()

	// Testing proper JSON body
	webhook := "{ \"webhookURL\": \"someURL.com\", \"baseCurrency\": \"EUR\", \"targetCurrency\": \"USD\", \"minTriggerValue\": 2.3, \"maxTriggerValue\":3.4}"

	resp, err := http.Post(ts.URL, "application/json", strings.NewReader(webhook))
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		all, _ := ioutil.ReadAll(resp.Body)
		t.Errorf("Expected StatusCode %d, received %d, Body: %s",
			http.StatusOK, resp.StatusCode, all)
	}
}

func TestPOSTAverage(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(routing))
	defer ts.Close()

	// Testing proper JSON body
	webhook := "{ \"baseCurrency\": \"EUR\", \"targetCurrency\": \"USD\"}"

	resp, err := http.Post(ts.URL+"/average", "application/json", strings.NewReader(webhook))
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		all, _ := ioutil.ReadAll(resp.Body)
		t.Errorf("Expected StatusCode %d, received %d, Body: %s",
			http.StatusOK, resp.StatusCode, all)
	}
}

func TestPOSTwebhookUrlInvoke(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(routing))
	defer ts.Close()

	// Testing proper JSON body
	webhook := "{ \"baseCurrency\": \"EUR\", \"targetCurrency\": \"USD\", \"currentRate\": 2.3, \"minTriggerValue\": 2.3, \"maxTriggerValue\":3.4}"

	resp, err := http.Post(ts.URL+"/webhookUrl", "application/json", strings.NewReader(webhook))
	if err != nil {
		t.Errorf("Error creating the POST request, %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		all, _ := ioutil.ReadAll(resp.Body)
		t.Errorf("Expected StatusCode %d, received %d, Body: %s",
			http.StatusOK, resp.StatusCode, all)
	}
}

func TestIsValidURL(t *testing.T) {
	//Should work
	url, _ := url.Parse("http://wwww.vg.no")
	res := isValidURL(url.String())
	if !res {
		t.Fail()
	}

	//Should fail
	url, _ = url.Parse("tullogfant.com")
	res = isValidURL(url.String())
	if res {
		t.Failed()
	}

	//Should fail
	url, _ = url.Parse("http://www.tullogfant.cp")
	res = isValidURL(url.String())
	if res {
		t.Failed()
	}
}

func TestGetData(t *testing.T) {
	p := Currency{}
	err := getData("api.fixer.io/latest?base=EUR", &p)

	if len(p.Rates) < 0 {
		t.Fail()
	}

	p = Currency{}
	err = getData("api.fixer.com", &p)
	if err == nil {
		t.Failed()
	}
}

/*func TestDB_getAverage(t *testing.T) {
	db := setUpDB("mongodb://localhost", "currency_db", "currency_latest")

	db.populateCurrency()

	average := db.getAverage("EUR", "NOK")
	if average < 0 {
		t.Fail()
	}

	average = db.getAverage("USD", "NOK")
	if average < 0 {
		t.Fail()
	}

	average = db.getAverage("TULL", "FANT")
	if average > 0 {
		t.Failed()
	}

	db.tearDownCollection()
}*/

func TestDB_FindAndInvoke(t *testing.T) {
	db := setUpDB("mongodb://localhost", "currency_db", "currency_latest")
	p := InvokeJSON{}
	p.BaseCurrency = "EUR"
	p.TargetCurrency = "NOK"
	p.MinTriggerValue = 2.3
	p.MaxTriggerValue = 4.5

	res := db.findAndInvoke(&p)
	if len(res) < 0 {
		t.Failed()
	}
}

/*func TestPayload_Invoke(t *testing.T) {
	type moq struct{}

	p := payload{}
	p.WebhookURL = "http://www.someurl.com"
	http.ResponseWriter()
	if !p.Invoke() {
		t.Fail()
	}

	p.WebhookURL = "someurl.com"
	if !p.Invoke() {
		t.Fail()
	}
}*/
